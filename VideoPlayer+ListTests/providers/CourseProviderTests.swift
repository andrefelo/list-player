//
//  CourseProviderTests.swift
//  VideoPlayer+ListTests
//
//  Created by Andres Lozano on 4/10/21.
//

import XCTest
@testable import VideoPlayer_List

class CourseProviderTests: XCTestCase {
  
  
  func test_get_course_list() throws {
    let expt = XCTestExpectation()
    let sut = CourseProvider(dataSource: .dummy)
    sut.getList { response in
      switch response {
        case .success(let result):
          XCTAssert(!result.isEmpty)
          XCTAssertTrue(true)
          expt.fulfill()
          break
        case .failure(let error):
          XCTAssertEqual(error.localizedDescription, CourseProviderError.cannotBeConvertToExpectationValue.localizedDescription)
          XCTFail("Expected to be a success but got a failure with: \(error.localizedDescription)\n\n")
          XCTAssertTrue(true)
          expt.fulfill()
          break
      }
    }
    wait(for: [expt], timeout: 10)
  }
}



