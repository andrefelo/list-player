  //
  //  ModelsTest.swift
  //  VideoPlayer+ListTests
  //
  //  Created by Andres Lozano on 3/10/21.
  //

import XCTest
@testable import VideoPlayer_List

class VideoModelTests: XCTestCase {
  
  
  func test_get_last_video_watched() throws {
      // Given a course with its video list, return a video with the most recent visit
    let sut = CourseModel(
      name: "First course",
      author: "Pepito perez",
      imageUrl: "https://www.petguide.com/wp-content/uploads/2019/09/can-i-put-vaseline-on-my-dog-300x200.jpg",
      videos: VideoModel.DummyList(withLastWatched: true)
    )
    let video = sut.getLastVideoWatched()
    XCTAssertNotNil(video)
    XCTAssertEqual(video?.position, 5)
  }
  
  
  func test_get_last_video_watched_without_visit() throws {
    // Given a course with its video list, return a video with the most recent visit
    // If all of them doesnt have last watched, return the first one
    let sut = CourseModel(
      name: "First course",
      author: "Pepito perez",
      imageUrl: "https://www.petguide.com/wp-content/uploads/2019/09/can-i-put-vaseline-on-my-dog-300x200.jpg",
      videos: VideoModel.DummyList()
    )
    let video = sut.getLastVideoWatched()
    XCTAssertNotNil(video)
    XCTAssertEqual(video?.position, 1)
  }
  
  
  func test_check_url_video() throws {
      // Given a video, check its url is equals with:
      // https://crehana-videos.akamaized.net/outputs/trailer/89ef7d652e4549709347f89aa7be0f57/1f68b3fffd1641c0b03d1457a53808d4.m3u8
    let sut = VideoModel(
      name: "Video test",
      url: "89ef7d652e4549709347f89aa7be0f57/1f68b3fffd1641c0b03d1457a53808d4.m3u8",
      duration: 10,
      size: 10,
      position: 1
    )
    
    XCTAssertEqual(
      sut.getURL()?.absoluteString,
      "https://crehana-videos.akamaized.net/outputs/trailer/89ef7d652e4549709347f89aa7be0f57/1f68b3fffd1641c0b03d1457a53808d4.m3u8"
    )
  }
}



