//
//  CourseListViewModel.swift
//  VideoPlayer+ListTests
//
//  Created by Andres Lozano on 6/10/21.
//

import XCTest
@testable import VideoPlayer_List

class CourseListViewModelTests: XCTestCase {

  var sut: CourseListViewModel!
  
  
  override func setUp() {
    sut = .init(coordinator: .init())
  }
  
  override func tearDown() {
    sut = nil
  }
  
  func test_fetchItems() throws {
    let expt = XCTestExpectation()
    sut.items.bind { result in
      XCTAssertNotNil(result)
      expt.fulfill()
    }
    sut.fetchItems()
    wait(for: [expt], timeout: 10)
  }

}


