  //
  //  CourseDetailViewModelTest.swift
  //  VideoPlayer+ListTests
  //
  //  Created by Andres Lozano on 6/10/21.
  //

import XCTest
@testable import VideoPlayer_List

class CourseDetailViewModelTest: XCTestCase {
  
  
  func test_check_number_videos() throws {
    let sut = courseDetailViewModel
    DispatchQueue.main.asyncAfter(deadline: .now()) {
      let expt = XCTestExpectation()
      sut.items.bind { result in
        XCTAssertEqual(result?.count, 5)
        expt.fulfill()
      }
      self.wait(for: [expt], timeout: 10)
    }
  }
  
  
  func test_select_video() throws {
    let sut = courseDetailViewModel
    DispatchQueue.main.asyncAfter(deadline: .now()) {
      let expt = XCTestExpectation()
      sut.onSelectVideo.bind { result in
        XCTAssertNotNil(result)
        expt.fulfill()
      }
      sut.didSelectItemAt(.init(item: 0, section: 1))
      self.wait(for: [expt], timeout: 10)
    }
  }
  
  
  
  func test_get_last_video_watched() throws {
    let sut = courseDetailViewModel
    DispatchQueue.main.asyncAfter(deadline: .now()) {
      let expt = XCTestExpectation()
      sut.onSelectVideo.bind { result in
        XCTAssertNotNil(result)
        expt.fulfill()
      }
      sut.getLastVideoWatched()
      self.wait(for: [expt], timeout: 10)
    }
  }
  
  
  var courseDetailViewModel: CourseDetailViewModel {
    let imageUrl = "https://www.petguide.com/wp-content/uploads/2019/09/can-i-put-vaseline-on-my-dog-300x200.jpg"
    return .init(
      coordinator: .init(),
      item: .init(name: "Javascript de principiante a experto", author: "Pepito perez", imageUrl: imageUrl, videos: VideoModel.DummyList(withLastWatched: true))
    )
  }
  
}
