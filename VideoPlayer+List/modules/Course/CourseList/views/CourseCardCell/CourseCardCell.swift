//
//  ProductList1Cell.swift
//  TestLibraries
//
//  Created by Andres Lozano on 29/06/21.
//


import UIKit
import ViewControllersAFL
import SDWebImage

class CourseCardCell: DefaultCollectionCell<CourseCardCellInfo> {
  
  
  // ---------------------------------------------------------------------
  // MARK: IBOutlets variables
  // ---------------------------------------------------------------------
  
  
  
  @IBOutlet var titleLabel: UILabel!
  @IBOutlet var descriptionLabel: UILabel!
  @IBOutlet var imageView: UIImageView!
  
  
  
  
  // ---------------------------------------------------------------------
  // MARK: Life cycle
  // ---------------------------------------------------------------------
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    imageView.cornerRadius = 5
  }
  
  
  override func prepareForReuse() {
    super.prepareForReuse()
    item = nil
  }
  
  
  // ---------------------------------------------------------------------
  // MARK: Helpers func
  // ---------------------------------------------------------------------
  
  
  override func setup() {
    guard let item = item else { return }
    descriptionLabel.text = item.getDescription()
    titleLabel.text = item.getTitle()
    DispatchQueue.main.async {
      self.imageView.sd_setImage(
        with: item.getUrlImage(),
        placeholderImage: UIImage(named: "placeholder")
      )
    }
  }
}



protocol CourseCardCellInfo {
  func getTitle() -> String
  func getDescription() -> String
  func getUrlImage() -> URL?
}
