//
//  CourseListViewModel.swift
//  VideoPlayer+List
//
//  Created by Andres Lozano on 4/10/21.
//

import Foundation
import ToolsAFL


class CourseListViewModel: NSObject {
  
  typealias ModelItem = CourseModel
  
  
    // ---------------------------------------------------------------------
    // MARK: Constants
    // ---------------------------------------------------------------------
  
  
  let coordinator: CourseCoordinator
  
  
    // ---------------------------------------------------------------------
    // MARK: Variables
    // ---------------------------------------------------------------------
  
  
  var items = Bindable<[ModelItem]>()
  var onError = Bindable<String>()
  var title: String {
    return "My Courses"
  }
  
  
  
    // ---------------------------------------------------------------------
    // MARK: Constructor
    // ---------------------------------------------------------------------
  
  
  init(coordinator: CourseCoordinator) {
    self.coordinator = coordinator
    super.init()
  }
  
  
    // ---------------------------------------------------------------------
    // MARK: Helpers func
    // ---------------------------------------------------------------------
  
  
  
  func fetchItems()  {
    let provider = CourseProvider()
    provider.getList { response in
      switch response{
        case .success(let result):
          self.items.value = result
          break
        case .failure(let error):
          self.onError.value = error.localizedDescription
      }
    }
  }
  
}



extension CourseListViewModel {
  
  
  func numberOfRowsInSection(_ section: Int) -> Int {
    return items.value?.count ?? 0
  }
  
  
  func numberOfSections() -> Int {
    return 1
  }
  
  
  func getItemAt(_ indexPath: IndexPath) -> ModelItem? {
    return items.value?[indexPath.item]
  }
  
  
  func didSelectItemAt(_ indexPath: IndexPath) {
    guard let item = getItemAt(indexPath) else { return }
    coordinator.detail(item: item)
  }
}
