//
//  CourseListController.swift
//  VideoPlayer+List
//
//  Created by Andres Lozano on 4/10/21.
//

import UIKit

import ViewControllersAFL
import UIKitExtensionsAFL

class CourseListController: DefaultCollectionController<CourseListViewModel> {

  override func viewDidLoad() {
    super.viewDidLoad()
    title = viewModel.title
    viewModel.fetchItems()
  }
  
  
  override func loadView() {
    super.loadView()
    setupCollection()
  }
  
  
    // ---------------------------------------------------------------------
    // MARK: Helpers func
    // ---------------------------------------------------------------------
  
  
  func setupCollection() {
    setupRefreshCtrlCollection()
    let space: CGFloat = 10
    
    minimumInterRow = 1
    collectionInsets = .init(
      top: space * 3,
      left: 0,
      bottom: space,
      right: 0
    )
    
    collectionView.registerFromNib(CourseCardCell.self)
    collectionView.backgroundColor = .black
    
    if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
      flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
    }
    
    view.backgroundColor = .black
  }
  
  
  override func setupObservers() {
    viewModel.items.bind { [weak self] _ in
      DispatchQueue.main.async {
        self?.collectionView.endRefreshing()
        self?.collectionView.reloadData()
      }
    }
    
    viewModel.onError.bind { [weak self] msj in
      self?.showAlert(title: "Opss", message: msj)
    }
    
    onRefresh = { [weak self] in
      self?.viewModel.fetchItems()
    }
  }
  
  
  func showAlert(title: String?, message: String?) {
    let alert = UIAlertController(
      title: title,
      message: message,
      preferredStyle: .alert
    )
    
    alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
    present(alert, animated: true)
  }
  
  
    // ---------------------------------------------------------------------
    // MARK: UICollectionViewDataSource
    // ---------------------------------------------------------------------
  
  
  override func numberOfSections(in collectionView: UICollectionView) -> Int {
    return viewModel.numberOfSections()
  }
  
  
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return viewModel.numberOfRowsInSection(section)
  }
  
  
  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell: CourseCardCell = collectionView.dequeueCell(indexPath: indexPath)
    cell.item = viewModel.getItemAt(indexPath)
    return cell
  }
  
  
  override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    viewModel.didSelectItemAt(indexPath)
  }

  
  
}
