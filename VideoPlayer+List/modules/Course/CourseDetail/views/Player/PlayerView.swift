  //
  //  PlayerView.swift
  //  VideoPlayer+List
  //
  //  Created by Andres Lozano on 5/10/21.
  //

import UIKit
import AVFoundation

class PlayerView: UIView, CustomViewProtocol {
  
  
  @IBOutlet var containerView: UIView!
  @IBOutlet var playerContainerView: UIView!
  @IBOutlet var containerButtonsView: UIView!
  @IBOutlet var playPauseButton: UIButton!
  @IBOutlet var durationLabel: UILabel!
  @IBOutlet var currentTimeLabel: UILabel!
  @IBOutlet var currentTimeSlider: UISlider!
  
  
    // ---------------------------------------------------------------------
    // MARK: Variables
    // ---------------------------------------------------------------------
  
  
  var timer: Timer?
  var player: AVPlayer?
  var playerLayer: AVPlayerLayer!
  
  
    // ---------------------------------------------------------------------
    // MARK: Constructor
    // ---------------------------------------------------------------------
  
  
  init() {
    player = .init()
    super.init(frame: .zero)
    commonInit(name: "PlayerView")
    setup()
  }
  
  
  required init?(coder: NSCoder) {
    player = .init()
    super.init(coder: coder)
    commonInit(name: "PlayerView")
    setup()
  }
  
  
    // ---------------------------------------------------------------------
    // MARK: Life cycle
    // ---------------------------------------------------------------------
  
  
  override func layoutSubviews() {
    super.layoutSubviews()
    playerLayer.frame = playerContainerView.bounds
  }
  
  
    // ---------------------------------------------------------------------
    // MARK: Private Helpers func
    // ---------------------------------------------------------------------
  
  
  private func setup() {
    setupContainerButtonsView()
    setupPlayer()
  }
  
  
  private func setupPlayer() {
    playerLayer = .init(player: player)
    playerLayer.videoGravity = .resizeAspectFill
    playerContainerView.layer.addSublayer(playerLayer)
    player?.addPeriodicTimeObserver(forInterval: .init(seconds: 0.5, preferredTimescale: CMTimeScale(NSEC_PER_SEC)), queue: .main) { [weak self] time in
      guard
        let self = self,
        let currentTime = self.player?.currentItem,
        currentTime.status == .readyToPlay
      else { return  }
      self.handleCurrentTime(item: currentTime)
    }
  }
  
  
  private func rewindVideo(by seconds: Float64) {
    guard let currentTime = player?.currentTime() else { return }
    var newTime = CMTimeGetSeconds(currentTime) - seconds
    if newTime <= 0 {
      newTime = 0
    }
    seek(value: Float(newTime))
  }
  
  
  private func forwardVideo(by seconds: Float64) {
    guard
      let player = player,
      let duration = player.currentItem?.duration
    else { return }
    let currentTime = player.currentTime()
    var newTime = CMTimeGetSeconds(currentTime) + seconds
    if newTime >= CMTimeGetSeconds(duration) {
      newTime = CMTimeGetSeconds(duration)
    }
    seek(value: Float(newTime))
  }
  
  
  private func setupContainerButtonsView() {
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handlerTapGestureRecognizer))
    tapGesture.numberOfTapsRequired = 1
    tapGesture.numberOfTouchesRequired = 1
    addGestureRecognizer(tapGesture)
    handleTimerAction()
  }
  
  
  @objc private func handlerTapGestureRecognizer(sender: UITapGestureRecognizer) {
    if !containerButtonsView.isHidden {
      return
    }
    handleTimerAction()
  }
  
  
  private func handleTimerAction() {
    timer?.invalidate()
    self.containerButtonsView.isHidden = false
    timer = Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { [weak self] _ in
      guard let self = self else { return }
      self.containerButtonsView.isHidden = true
    }
  }
  
  
  private func handleCurrentTime(item: AVPlayerItem) {
    let seconds = item.duration.seconds
    currentTimeSlider.maximumValue = Float(seconds)
    currentTimeSlider.minimumValue = 0
    currentTimeSlider.value = Float(item.currentTime().seconds)
    currentTimeLabel.text = timeToSeconds(value: item.currentTime())
  }
  
  
  fileprivate func seek(value: Float) {
    player?.seek(to: CMTimeMake(value: .init(value * 1000), timescale: 1000))
  }
  
  
    // ---------------------------------------------------------------------
    // MARK: Public Helpers func
    // ---------------------------------------------------------------------
  
  
  func setUrlPlayer(url: URL, withPlay play: Bool = false){
    pause()
    player?.replaceCurrentItem(with: nil)
    player?.replaceCurrentItem(with: .init(url: url))
    if play {
      player?.play()
    }
    player?.currentItem?.addObserver(self, forKeyPath: "duration", options: [.new, .initial], context: nil)
  }
  
  
  func pause() {
    player?.pause()
  }
  
  
  func play() {
    player?.play()
  }
  
  deinit {
    player?.currentItem?.removeObserver(self, forKeyPath: "duration")
    player = nil
  }
}



extension PlayerView {
  
  
    // ---------------------------------------------------------------------
    // MARK: IBActions
    // ---------------------------------------------------------------------
  
  
  @IBAction func backwardButtonAction(_ sender: Any) {
    handleTimerAction()
    rewindVideo(by: 10)
  }
  
  
  @IBAction func forwardButtonAction(_ sender: Any) {
    handleTimerAction()
    forwardVideo(by: 10)
  }
  
  
  @IBAction func currentTimeSliderChanged(_ sender: UISlider) {
    seek(value: sender.value)
  }
  
  
  @IBAction func playPauseAction(_ sender: Any) {
    handleTimerAction()
    let isPaused = player?.timeControlStatus == .paused
    let image = isPaused ? "SF_play" : "SF_pause"
    playPauseButton.setImage(.init(named: image), for: [])
    isPaused ? play() : pause()
  }

}



extension PlayerView {
  
    // ---------------------------------------------------------------------
    // MARK: Observers
    // ---------------------------------------------------------------------
  

  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    guard
      keyPath == "duration",
      let player = player,
      let durarion = player.currentItem?.duration.seconds,
      durarion > 0.0
    else { return  }
    durationLabel.text = timeToSeconds(value: player.currentItem!.duration)
  }
  
  
  fileprivate func timeToSeconds(value: CMTime) -> String {
    let totalSecods = CMTimeGetSeconds(value)
    let hours = Int(totalSecods / 3600)
    let minutes = Int(totalSecods / 60 ) % 60
    let seconds = Int(totalSecods.truncatingRemainder(dividingBy: 60))
    if hours > 0 {
      return String(format: "%i:%02i:%02i", hours, minutes, seconds)
    } else {
      return String(format: "%02i:%02i", minutes, seconds)
    }
  }
}
