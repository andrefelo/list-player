  //
  //  VideoCardCell.swift
  //  VideoPlayer+List
  //
  //  Created by Andres Lozano on 5/10/21.
  //

import UIKit
import ViewControllersAFL

class VideoCardCell: DefaultCollectionCell<VideoCardCellInfo> {
  
  
    // ---------------------------------------------------------------------
    // MARK: IBOutlets variables
    // ---------------------------------------------------------------------
  
  
  @IBOutlet var titleLabel: UILabel!
  @IBOutlet var descriptionLabel: UILabel!
  @IBOutlet var circleView: UIView!
  @IBOutlet var lineView: UIView!
  
  
    // ---------------------------------------------------------------------
    // MARK: Life cycle
    // ---------------------------------------------------------------------
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    circleView.cornerRadius = circleView.frame.width / 2
  }
  
  
  override func prepareForReuse() {
    super.prepareForReuse()
    item = nil
  }
  
  
    // ---------------------------------------------------------------------
    // MARK: Helpers func
    // ---------------------------------------------------------------------
  
  
  override func setup() {
    guard let item = item else { return }
    descriptionLabel.text = item.getDescription()
    titleLabel.text = item.getTitle()
  }
  
  
  func toggleLineView(show: Bool) {
    lineView.alpha = show ? 1 : 0
  }
}



protocol VideoCardCellInfo {
  func getTitle() -> String
  func getDescription() -> String
}
