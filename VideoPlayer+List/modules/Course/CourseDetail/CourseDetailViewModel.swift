  //
  //  CourseDetailViewModel.swift
  //  VideoPlayer+List
  //
  //  Created by Andres Lozano on 5/10/21.
  //

import Foundation
import ToolsAFL

class CourseDetailViewModel: NSObject {
  
  typealias ModelItem = VideoModel
  
  
    // ---------------------------------------------------------------------
    // MARK: Constants
    // ---------------------------------------------------------------------
  
  
  let coordinator: CourseCoordinator
  let course: CourseModel
  
  
    // ---------------------------------------------------------------------
    // MARK: Variables
    // ---------------------------------------------------------------------
  
  
  var items = Bindable<[ModelItem]>()
  var onSelectVideo = Bindable<URL>()
  var onError = Bindable<String>()
  var title: String {
    return course.getTitle()
  }
  
  
  
    // ---------------------------------------------------------------------
    // MARK: Constructor
    // ---------------------------------------------------------------------
  
  
  init(coordinator: CourseCoordinator, item: CourseModel) {
    self.course = item
    self.coordinator = coordinator
    super.init()
  }
  
  
  func getLastVideoWatched() {
    items.value = course.getVideos()
    guard
      let video = course.getLastVideoWatched(),
      let url = video.getURL(),
      onSelectVideo.value != url
    else { return }
    onSelectVideo.value = url
  }
}



extension CourseDetailViewModel {
  
  
  func numberOfRowsInSection(_ section: Int) -> Int {
    return items.value?.count ?? 0
  }
  
  
  func numberOfSections() -> Int {
    return 1
  }
  
  
  func getItemAt(_ indexPath: IndexPath) -> ModelItem? {
    return items.value?[indexPath.item]
  }
  
  
  func didSelectItemAt(_ indexPath: IndexPath) {
    guard
      let item = getItemAt(indexPath),
      let url = item.getURL(),
      onSelectVideo.value != url
    else { return }
    item.setLastWatched(.init())
    onSelectVideo.value = url
  }
  
  
  func isLastItem(_ indexPath: IndexPath) -> Bool {
    guard let items = items.value else { return false }
    return (items.count - 1) == indexPath.item
  }
  
  
  func dismiss() {
    coordinator.dismiss()
  }
}
