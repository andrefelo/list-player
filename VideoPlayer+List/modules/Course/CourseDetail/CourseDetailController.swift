  //
  //  CourseDetailController.swift
  //  VideoPlayer+List
  //
  //  Created by Andres Lozano on 5/10/21.
  //

import UIKit
import ViewControllersAFL

class CourseDetailController: ViewController<CourseDetailViewModel> {
  
    // ---------------------------------------------------------------------
    // MARK: IBOutlets variables
    // ---------------------------------------------------------------------
  
  
  @IBOutlet var collectionView: DefaultCollection!
  @IBOutlet var playerView: PlayerView!
  @IBOutlet var titleCourseLabel: UILabel!
  @IBOutlet var authorLabel: UILabel!
  @IBOutlet var containerStackView: UIStackView!
  @IBOutlet var infoCourseStackView: UIStackView!
  @IBOutlet var playerheightContraint: NSLayoutConstraint!
  
  
    // ---------------------------------------------------------------------
    // MARK: Variables
    // ---------------------------------------------------------------------
  
  
  
  var statusBarOrientation: UIInterfaceOrientation? {
    get {
      guard let orientation = UIApplication.shared.windows.first?.windowScene?.interfaceOrientation else {
        #if DEBUG
        fatalError("Could not obtain UIInterfaceOrientation from a valid windowScene")
        #else
        return nil
        #endif
      }
      return orientation
    }
  }
  
  
    // ---------------------------------------------------------------------
    // MARK: Life cycle
    // ---------------------------------------------------------------------
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    handleDeviceLandscape()
  }
  
  
  override func viewDidAppear(_ animated: Bool) {
    self.setupCollection()
    super.viewDidAppear(animated)
  }
  
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    handleDeviceLandscape()
  }
  
  
    // ---------------------------------------------------------------------
    // MARK: Helpers func
    // ---------------------------------------------------------------------
  
  
  private func setup() {
    title = viewModel.title
    view.backgroundColor = .black
    poputateLabels()
    setupObs()
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
      self.viewModel.getLastVideoWatched()
    }
  }
  
  
  func setupCollection() {
    if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
      flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
    }
    collectionView.collectionInsets = .init(
      top: 0,
      left: 0,
      bottom: 20,
      right: 0
    )
    collectionView.minimumInterRow = 0
    collectionView.registerFromNib(VideoCardCell.self)
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.backgroundColor = .black
  }
  
  
  private func handleDeviceLandscape(){
    let isLandscape = UIDevice.current.orientation.isLandscape  == true
    collectionView.isHidden = isLandscape
    infoCourseStackView.isHidden = isLandscape
    playerheightContraint.isActive = isLandscape ? false : true
  }
  
  
  func setupObs() {
    
    viewModel.items.bind { [weak self] items in
      guard let self = self else { return }
      
      self.collectionView.reloadData()
    }
    
    viewModel.onSelectVideo.bind { [weak self] url in
      
      guard
        let self = self,
        let url = url
      else { return }
      
      self.playerView.setUrlPlayer(url: url, withPlay: true)
    }
  }
  
  
  private func poputateLabels() {
    titleCourseLabel.text = viewModel.course.getTitle()
    authorLabel.text = viewModel.course.author
  }
  
  
  @IBAction func closeButtonAction(_ sender: Any) {
    viewModel.dismiss()
  }
}


extension CourseDetailController: UICollectionViewDelegate, UICollectionViewDataSource {
  
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return viewModel.numberOfSections()
  }
  
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return viewModel.numberOfRowsInSection(section)
  }
  
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell: VideoCardCell = collectionView.dequeueCell(indexPath: indexPath)
    cell.item = viewModel.getItemAt(indexPath)
    cell.toggleLineView(show: !viewModel.isLastItem(indexPath))
    return cell
  }
  
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    viewModel.didSelectItemAt(indexPath)
  }
}
