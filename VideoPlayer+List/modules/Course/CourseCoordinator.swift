  //
  //  CourseCoordinator.swift
  //  VideoPlayer+List
  //
  //  Created by Andres Lozano on 4/10/21.
  //

import UIKit


class CourseCoordinator: Coordinator {
  
    // ---------------------------------------------------------------------
    // MARK: Variables
    // ---------------------------------------------------------------------
  
  
  var navigationController: UINavigationController
  
  
    // ---------------------------------------------------------------------
    // MARK: Constructor
    // ---------------------------------------------------------------------
  
  
  init() {
    navigationController = .init()
    navigationController.navigationBar.barStyle = .default
    navigationController.navigationBar.isTranslucent = true
    navigationController.navigationBar.prefersLargeTitles = true
  }
  
  
    // ---------------------------------------------------------------------
    // MARK: Helpers func
    // ---------------------------------------------------------------------
  
  
  
  
  func start(animated: Bool = false) {
    let vm = CourseListViewModel(coordinator: self)
    let ctrl = CourseListController(viewModel: vm)
    navigationController.pushViewController(ctrl, animated: animated)
  }
  
  
  func detail(item: CourseModel) {
    let vm = CourseDetailViewModel(coordinator: self, item: item)
    let ctrl = CourseDetailController(viewModel: vm)
    ctrl.modalPresentationStyle = .fullScreen
    present(ctrl: ctrl)
  }
}
