  //
  //  CourseModel.swift
  //  VideoPlayer+List
  //
  //  Created by Andres Lozano on 3/10/21.
  //

import Foundation

class CourseModel: Codable {
  
  
    // ---------------------------------------------------------------------
    // MARK: Properties
    // ---------------------------------------------------------------------
  
  
  private (set) var name: String
  private (set) var author: String
  private (set) var imageUrl: String
  private var videos: [VideoModel] = []
  
  
    // ---------------------------------------------------------------------
    // MARK: Constructor
    // ---------------------------------------------------------------------
  

  init(name: String, author: String, imageUrl: String, videos: [VideoModel] = []) {
    self.name = name
    self.author = author
    self.imageUrl = imageUrl
    self.videos = videos
  }
  
  
    // ---------------------------------------------------------------------
    // MARK: Helpers func
    // ---------------------------------------------------------------------
  
  
  func getLastVideoWatched() ->  VideoModel? {
    
    var video: VideoModel?
    
    for item in videos {
      if video == nil {
        video = item
      } else if video?.lastWatchedIsLessThan(item.lastWatched) == true {
        video = item
      }
    }
    
    if video?.lastWatched == nil {
      video = getVideos().first
    }
    
    return video
  }
  
  
  func getVideos() -> [VideoModel] {
    videos.sorted(by: { $0.position < $1.position })
  }
  
  
  func setVideos(_ value: [VideoModel]) {
    videos = value
  }
}

extension CourseModel: CourseCardCellInfo {
  func getTitle() -> String {
    name
  }
  
  func getDescription() -> String {
    author
  }
  
  func getUrlImage() -> URL? {
    .init(string: imageUrl)
  }
  
  
}
