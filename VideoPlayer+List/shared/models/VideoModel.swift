  //
  //  VideoModel.swift
  //  VideoPlayer+List
  //
  //  Created by Andres Lozano on 3/10/21.
  //

import Foundation

class VideoModel: Codable {
  
  
    // ---------------------------------------------------------------------
    // MARK: Properties
    // ---------------------------------------------------------------------
  
  
  private (set) var name: String
  private (set) var url: String
  private (set) var duration: Double
  private (set) var size: Double
  private (set) var lastWatched: Date?
  private (set) var position: Int
  
  
  init(
  name: String,
  url: String,
  duration: Double,
  size: Double,
  lastWatched: Date? = nil,
  position: Int
  ) {
    
    self.name = name
    self.url = url
    self.duration = duration
    self.size = size
    self.lastWatched = lastWatched
    self.position = position
  }
  
  
    // ---------------------------------------------------------------------
    // MARK: Helpers func
    // ---------------------------------------------------------------------
  
  
  func setLastWatched(_ value: Date) {
    lastWatched = value
  }
  
  
  func lastWatchedIsLessThan(_ value: Date?) -> Bool{
    
    guard
      let lastWatched = lastWatched
    else {
      return true
    }
    
    guard
      let value = value
    else {
      return false
    }
    
    return lastWatched < value
  }
  
  
  func getURL() -> URL? {
    let baseURL = "https://crehana-videos.akamaized.net/outputs/trailer/"
    return .init(string: baseURL + url)
  }
  
}


extension VideoModel: VideoCardCellInfo {
  func getTitle() -> String {
    name
  }
  
  func getDescription() -> String {
    "\(duration)m (\(size)mb)"
  }
  
  
}



extension VideoModel {
  
  static func DummyList(withLastWatched: Bool = false) -> [VideoModel] {
    
    let video1 = VideoModel(
      name: "Video Uno",
      url: "89ef7d652e4549709347f89aa7be0f57/1f68b3fffd1641c0b03d1457a53808d4.m3u8",
      duration: 3,
      size: 10,
      lastWatched: nil,
      position: 1
    )
    
    let video2 = VideoModel(
      name: "Video Dos",
      url: "29f6759c6bd04a5f9cdd4e351fc3ddde/d0da0c55b987479fafd7fb68ca298e5f.m3u8",
      duration: 4,
      size: 12,
      lastWatched: nil,
      position: 2
    )
    
    let video3 = VideoModel(
      name: "Video tres",
      url: "d6fa2199a76c4a62bad13d2431797061/34d25cc19c4b4e9f9a967fc9c2ed2902.m3u8",
      duration: 5,
      size: 13,
      lastWatched: withLastWatched ? Date() : nil,
      position: 3
    )
    
    let video4 = VideoModel(
      name: "Video cuatro",
      url: "e0a7d08a579240a2bcf4d9051e625977/7ae20874869443c2be01f672424e85b0.m3u8",
      duration: 8,
      size: 16,
      lastWatched: nil,
      position: 4
    )
    
    let video5 = VideoModel(
      name: "Video cinco",
      url: "1936e256200f43068f7bbc0aaba236c9/e188e88593d54c55be8150426eed4158.m3u8",
      duration: 6,
      size: 14,
      lastWatched: withLastWatched ? Date().addingTimeInterval(10) : nil,
      position: 5
    )
    
    return [video5, video3, video2, video4, video1]
  }
  
}
