//
//  CustomViewProtocol.swift
//  VideoPlayer+List
//
//  Created by Andres Lozano on 6/10/21.
//

import UIKit


protocol CustomViewProtocol {
  
  var containerView: UIView! { get }
  func commonInit(name: String)
  
}



extension CustomViewProtocol where  Self: UIView {
  
  func commonInit(name: String) {
    Bundle.main.loadNibNamed(name, owner: self, options: nil)
    addSubview(containerView)
    containerView.backgroundColor = .clear
    containerView.frame = bounds
    containerView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
  }
  
}
