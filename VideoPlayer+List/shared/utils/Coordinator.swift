//
//  Coordinator.swift
//  VideoPlayer+List
//
//  Created by Andres Lozano on 5/10/21.
//

import UIKit


protocol Coordinator {
  var navigationController: UINavigationController { get set }
  func start(animated: Bool) -> Void
}

extension Coordinator {
  
  func dismiss(animared: Bool = true) {
    navigationController.dismiss(animated: animared, completion: nil)
  }
  
  
  func pop(animared: Bool = true) {
    navigationController.popViewController(animated: animared)
  }
  
  
  func push(ctrl: UIViewController, animared: Bool = true) {
    navigationController.pushViewController(ctrl, animated: animared)
  }
  
  func present(ctrl: UIViewController, animared: Bool = true) {
    navigationController.present(ctrl, animated: animared, completion: nil)
  }
}
