//
//  CourseProvider+Helpers.swift
//  VideoPlayer+List
//
//  Created by Andres Lozano on 4/10/21.
//

import Foundation


typealias HandleSuccess<T:Codable, E: Error> =  (Result<T, E>) -> Void


protocol CourseProviderProtocol {
  func getList<T, E>(handleSuccess: @escaping HandleSuccess<T, E>)
}


enum CourseProviderError: LocalizedError, Error {
  case cannotBeConvertToExpectationValue
  
  var errorDescription: String? {
    switch self {
      case .cannotBeConvertToExpectationValue:
        return "cannot be convert to expectation value"
    }
  }
  
  
}
