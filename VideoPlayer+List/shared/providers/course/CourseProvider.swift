  //
  //  CourseProvider.swift
  //  VideoPlayer+List
  //
  //  Created by Andres Lozano on 4/10/21.
  //

import Foundation


class CourseProvider  {
  
    // ---------------------------------------------------------------------
    // MARK: Porperties
    // ---------------------------------------------------------------------
  
  
  private (set) var dataSource: CourseProviderProtocol
  
  
    // ---------------------------------------------------------------------
    // MARK: Constructor
    // ---------------------------------------------------------------------
  
  
  init(dataSource: DataSource = .dummy) {
    self.dataSource = dataSource.dataSource
  }
  
  
    // ---------------------------------------------------------------------
    // MARK: Helpers func
    // ---------------------------------------------------------------------
  
  
  func getList(success: @escaping HandleSuccess<[CourseModel], Error>) {
    self.dataSource.getList(handleSuccess: success)
  }
}



extension CourseProvider {
  
  enum DataSource {
    case dummy
    
    
    var dataSource: CourseProviderProtocol {
      switch self {
        case .dummy:
          return CourseDummy()
      }
    }
    
  }
  
  
}
