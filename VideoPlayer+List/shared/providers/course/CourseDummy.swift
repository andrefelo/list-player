  //
  //  CourseDummy.swift
  //  VideoPlayer+List
  //
  //  Created by Andres Lozano on 4/10/21.
  //

import Foundation


class CourseDummy: CourseProviderProtocol {
  
  
  func getList<T, E>(handleSuccess: @escaping HandleSuccess<T, E>) {
    let imageUrl = "https://www.petguide.com/wp-content/uploads/2019/09/can-i-put-vaseline-on-my-dog-300x200.jpg"
    let items: [CourseModel] = [
      .init(name: "Javascript de principiante a experto", author: "Pepito perez", imageUrl: imageUrl, videos: VideoModel.DummyList(withLastWatched: true)),
      .init(name: "Bigdata con Pyhton", author: "Pedro Picapiedra", imageUrl: imageUrl, videos: VideoModel.DummyList(withLastWatched: false)),
      .init(name: "Reacnative de 0 a experto", author: "Pepito", imageUrl: imageUrl, videos: VideoModel.DummyList(withLastWatched: true)),
      .init(name: "React de intermedio a experto", author: "Pepito", imageUrl: imageUrl, videos: VideoModel.DummyList(withLastWatched: true)),
      .init(name: "Realidad aumentada en Android de principiante a avanzado", author: "Pepito", imageUrl: imageUrl, videos: VideoModel.DummyList(withLastWatched: true)),
    ]
    guard let value = items as? T else {
      handleSuccess(.failure( CourseProviderError.cannotBeConvertToExpectationValue as! E ))
      return
    }
    handleSuccess(.success( value ))
  }
}


