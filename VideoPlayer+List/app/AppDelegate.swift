//
//  AppDelegate.swift
//  VideoPlayer+List
//
//  Created by Andres Lozano on 3/10/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {


  var window: UIWindow?
  var mainCoordinator: Coordinator?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    setupNavigationController()
    mainCoordinator = CourseCoordinator()
    guard #available(iOS 13, *) else {
      window = UIWindow()
      window?.rootViewController = mainCoordinator?.navigationController
      window?.makeKeyAndVisible()
      mainCoordinator?.start(animated: false)
      return true
    }
    
    return true
  }

  // MARK: UISceneSession Lifecycle

  func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
  }

  func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
  }
  
  
  private func setupNavigationController() {
    let navigationBarTitleTextAttributes :[NSAttributedString.Key : Any] = [
      NSAttributedString.Key.foregroundColor: UIColor.white
    ]
    UINavigationBar.appearance().barTintColor = .clear
    UINavigationBar.appearance().tintColor = .white
    UINavigationBar.appearance().titleTextAttributes = navigationBarTitleTextAttributes
    UINavigationBar.appearance().largeTitleTextAttributes = navigationBarTitleTextAttributes
  }


}

